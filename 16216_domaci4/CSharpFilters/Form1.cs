using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using HWFilters;
using System.IO;
using HWFilters.Compression;

namespace CSharpFilters
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{

        private bool ditherGS = false;

		private System.Drawing.Bitmap m_Bitmap;
		private System.Drawing.Bitmap m_Undo;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem FileLoad;
		private System.Windows.Forms.MenuItem FileSave;
		private System.Windows.Forms.MenuItem FileExit;
        private double Zoom = 1.0;
		private System.Windows.Forms.MenuItem menuItem5;
        private System.Windows.Forms.MenuItem Undo;
        private MenuItem menuItem6;
        private MenuItem HW_Fitmap_Downsample;
        private MenuItem HW_Fitmap_Load;
        private MenuItem HW_Fitmap_FullHuffman;
        private MenuItem HW_Fitmap_LoadFullHuffman;
        private MenuItem menuItem2;
        private MenuItem menuItem3;
        private IContainer components;

		public Form1()
		{
			InitializeComponent();

			m_Bitmap= new Bitmap(2, 2);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		        #region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.FileLoad = new System.Windows.Forms.MenuItem();
            this.FileSave = new System.Windows.Forms.MenuItem();
            this.FileExit = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.Undo = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.HW_Fitmap_Downsample = new System.Windows.Forms.MenuItem();
            this.HW_Fitmap_Load = new System.Windows.Forms.MenuItem();
            this.HW_Fitmap_FullHuffman = new System.Windows.Forms.MenuItem();
            this.HW_Fitmap_LoadFullHuffman = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem5,
            this.menuItem6});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileLoad,
            this.FileSave,
            this.FileExit});
            this.menuItem1.Text = "File";
            // 
            // FileLoad
            // 
            this.FileLoad.Index = 0;
            this.FileLoad.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
            this.FileLoad.Text = "Load";
            this.FileLoad.Click += new System.EventHandler(this.File_Load);
            // 
            // FileSave
            // 
            this.FileSave.Index = 1;
            this.FileSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.FileSave.Text = "Save";
            this.FileSave.Click += new System.EventHandler(this.File_Save);
            // 
            // FileExit
            // 
            this.FileExit.Index = 2;
            this.FileExit.Text = "Exit";
            this.FileExit.Click += new System.EventHandler(this.File_Exit);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 1;
            this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.Undo});
            this.menuItem5.Text = "Edit";
            // 
            // Undo
            // 
            this.Undo.Index = 0;
            this.Undo.Text = "Undo";
            this.Undo.Click += new System.EventHandler(this.OnUndo);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 2;
            this.menuItem6.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.HW_Fitmap_Downsample,
            this.HW_Fitmap_Load,
            this.HW_Fitmap_FullHuffman,
            this.HW_Fitmap_LoadFullHuffman,
            this.menuItem2,
            this.menuItem3});
            this.menuItem6.Text = "Function";
            // 
            // HW_Fitmap_Downsample
            // 
            this.HW_Fitmap_Downsample.Index = 0;
            this.HW_Fitmap_Downsample.Text = "Downsample";
            this.HW_Fitmap_Downsample.Click += new System.EventHandler(this.HW_Filter_Fitmap_Downsample);
            // 
            // HW_Fitmap_Load
            // 
            this.HW_Fitmap_Load.Index = 1;
            this.HW_Fitmap_Load.Text = "Load";
            this.HW_Fitmap_Load.Click += new System.EventHandler(this.HW_Filter_Fitmap_Load);
            // 
            // HW_Fitmap_FullHuffman
            // 
            this.HW_Fitmap_FullHuffman.Index = 2;
            this.HW_Fitmap_FullHuffman.Text = "FullHuffman";
            this.HW_Fitmap_FullHuffman.Click += new System.EventHandler(this.HW_Filter_Fitmap_FullHuffman);
            // 
            // HW_Fitmap_LoadFullHuffman
            // 
            this.HW_Fitmap_LoadFullHuffman.Index = 3;
            this.HW_Fitmap_LoadFullHuffman.Text = "LoadFullHuffman";
            this.HW_Fitmap_LoadFullHuffman.Click += new System.EventHandler(this.HW_Filter_Fitmap_LoadFullHuffman);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 4;
            this.menuItem2.Text = "HalftoningMat2";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 5;
            this.menuItem3.Text = "HalftoningMat3";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(616, 347);
            this.MaximumSize = new System.Drawing.Size(2304, 1246);
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "Image Filters ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
            byte[] bytes = new byte[]
            {
                255, 0, 20, 20, 13, 14, 18, 255, 0, 0, 0, 0, 1, 2, 1, 255, 255, 255, 255, 255, 255
            };
            HuffmanCode<byte> hCode = new HuffmanCode<byte>(bytes);

            Application.Run(new Form1());
		}

		protected override void OnPaint (PaintEventArgs e)
		{
			Graphics g = e.Graphics;

			g.DrawImage(m_Bitmap, new Rectangle(this.AutoScrollPosition.X, this.AutoScrollPosition.Y, (int)(m_Bitmap.Width*Zoom), (int)(m_Bitmap.Height * Zoom)));
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
		}

		private void File_Load(object sender, System.EventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();

			openFileDialog.InitialDirectory = "c:\\" ;
			openFileDialog.Filter = "Bitmap files (*.bmp)|*.bmp|Jpeg files (*.jpg)|*.jpg|GIF files(*.gif)|*.gif|PNG files(*.png)|*.png|All valid files|*.bmp/*.jpg/*.gif/*.png";
			openFileDialog.FilterIndex = 2 ;
			openFileDialog.RestoreDirectory = true ;

			if(DialogResult.OK == openFileDialog.ShowDialog())
			{
				m_Bitmap = (Bitmap)Bitmap.FromFile(openFileDialog.FileName, false);
                if (m_Bitmap.Size.Width > this.MaximumSize.Width || m_Bitmap.Size.Height > this.MaximumSize.Height)
                {
                    this.AutoScroll = true;
                    this.AutoScrollMinSize = new Size((int)(m_Bitmap.Width * Zoom), (int)(m_Bitmap.Height * Zoom));
                } else
                {
                    this.Size = m_Bitmap.Size;
                }

				this.Invalidate();
			}
		}

		private void File_Save(object sender, System.EventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();

			saveFileDialog.InitialDirectory = "c:\\" ;
			saveFileDialog.Filter = "Bitmap files (*.bmp)|*.bmp|Jpeg files (*.jpg)|*.jpg|All valid files (*.bmp/*.jpg)|*.bmp/*.jpg" ;
			saveFileDialog.FilterIndex = 1 ;
			saveFileDialog.RestoreDirectory = true ;

			if(DialogResult.OK == saveFileDialog.ShowDialog())
			{
				m_Bitmap.Save(saveFileDialog.FileName);
			}
		}

		private void File_Exit(object sender, System.EventArgs e)
		{
			this.Close();
		}

	
				

		delegate bool ImageProcessingMethod(Bitmap b);

		private void ProcessImage(ImageProcessingMethod methodToExecute, Bitmap b)
		{
			m_Undo = (Bitmap)m_Bitmap.Clone();
			if (methodToExecute(b))
				this.Invalidate();
		}        

		private void OnUndo(object sender, System.EventArgs e)
		{
			Bitmap temp = (Bitmap)m_Bitmap.Clone();
			m_Bitmap = (Bitmap)m_Undo.Clone();
			m_Undo = (Bitmap)temp.Clone();
			this.Invalidate();
		}
				

    


        private void HW_Filter_Fitmap_Downsample(object sender, EventArgs e)
        {
            Console.WriteLine($"Fitmap downsample");
            Fitmap f = Fitmap.SaveBitmap(m_Bitmap, "D:/file.fmp");
            m_Bitmap = f.GetBitmap();
            m_Bitmap.Save("D:/bmpfmp1NoComp.bmp", ImageFormat.Bmp);
            this.Invalidate();
        }

        private void HW_Filter_Fitmap_Load(object sender, EventArgs e)
        {
            Console.WriteLine($"Fitmap Load");
            m_Bitmap = Fitmap.LoadBitmapFromFile("D:/file.fmp");
            this.Invalidate();
        }

        private void HW_Filter_Fitmap_FullHuffman(object sender, EventArgs e)
        {
            Console.WriteLine($"Fitmap Full Huffman");
            Fitmap f = new Fitmap(m_Bitmap);//downsempling 
            f.SetCompressionAlg(FitmapCompression.CompressionType.HuffmanFull);//full haffan
            f.SaveToFile("D:/file.fmp1");//haffman i save
            m_Bitmap = f.GetBitmap();
            this.Invalidate();
        }

        private void HW_Filter_Fitmap_LoadFullHuffman(object sender, EventArgs e)
        {
            Console.WriteLine($"Fitmap Full Huffman Load");
            m_Bitmap = Fitmap.LoadBitmapFromFile("D:/file.fmp1");
            this.Invalidate();
        }

        private void HW_Filter_Fitmap_ChannelHuffman(object sender, EventArgs e)
        {
            Console.WriteLine($"Fitmap Channel Huffman");
            Fitmap f = new Fitmap(m_Bitmap);
            f.SetCompressionAlg(FitmapCompression.CompressionType.HuffmanPerChannel);
            f.SaveToFile("D:/file.fmp2");
            m_Bitmap = f.GetBitmap();
            this.Invalidate();
        }

        private void HW_Filter_Fitmap_LoadChannelHuffman(object sender, EventArgs e)
        {
            Console.WriteLine($"Load Fitmap Channel Huffman");
            m_Bitmap = Fitmap.LoadBitmapFromFile("D:/file.fmp2");
            this.Invalidate();
        }


        private void HW_Filter_Dither_ToggleGS(object sender, EventArgs e)
        {
            ditherGS = !ditherGS;
            Console.WriteLine($"Grayscale " + (ditherGS ? "on" : "off"));
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {          

                m_Undo = (Bitmap)m_Bitmap.Clone();
                if (Halftoning.HalftoningUnsafe(m_Bitmap,2))
                    this.Invalidate();
            
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            m_Undo = (Bitmap)m_Bitmap.Clone();
            if (Halftoning.HalftoningUnsafe(m_Bitmap, 3))
                this.Invalidate();
        }
    }
}

