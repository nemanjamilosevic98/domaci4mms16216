﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
namespace CSharpFilters
{
    public static class Halftoning
    {
       
        public static unsafe bool HalftoningUnsafe(Bitmap img, int n)
        {
            BitmapData bmData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* firstPixel = (byte*)(void*)Scan0;
                byte* p = (byte*)(void*)Scan0;
                //int nOffset = stride - img.Width * 3;
                int nWidth = img.Width * 3;

                if (n != 3)
                {
                    for (int y = 0; y < img.Height; y += 2)
                    {
                        for (int x = 0; x < nWidth; x += 6)
                        {
                            //pattern2(p, bmData.Stride); //b
                            //pattern2(p+1, bmData.Stride); //g
                            //pattern2(p+2, bmData.Stride); //r

                            //pattern2
                            for (int i = 0; i < 3; i++)
                            {
                                int color = 0;
                                color += p[x + i];
                                color += p[x + 3 + i];
                                color += p[x + stride + i];
                                color += p[x + stride + 3 + i];

                                //color <= 0.2 * 1200
                                p[x + i] = 0;
                                p[x + 3 + i] = 0;
                                p[x + stride + i] = 0;
                                p[x + stride + 3 + i] = 0;

                                if (color > 0.2 * 1020)
                                    p[x + stride + i] = 255;
                                if (color > 0.4 * 1020)
                                    p[x + 3 + i] = 255;
                                if (color > 0.6 * 1020)
                                    p[x + stride + 3 + i] = 255;
                                if (color > 0.8 * 1020)
                                    p[x + i] = 255;

                            }
                        }
                        p = firstPixel + (y * bmData.Stride);
                    }
                }
                else
                {
                    for (int y = 0; y < img.Height; y += 3)
                    {
                        for (int x = 0; x < nWidth; x += 9)
                        {                            
                            for (int i = 0; i < 3; i++)
                            {
                                int color = 0;
                                color += p[x + i];  
                                color += p[x + 3 + i];  
                                color += p[x + 6 + i]; 

                                color += p[x + stride + i];  
                                color += p[x + stride + 3 + i];  
                                color += p[x + stride + 6 + i]; 

                                color += p[x + 2 * stride + i];  
                                color += p[x + 2 * stride + 3 + i];  
                                color += p[x + 2 * stride + 6 + i]; 


                                //color <= 0.1 * 2295
                                p[x + i] = 0;  
                                p[x + 3 + i] = 0;  
                                p[x + 6 + i] = 0; 

                                p[x + stride + i] = 0;  
                                p[x + stride + 3 + i] = 0;  
                                p[x + stride + 6 + i] = 0; 

                                p[x + stride + i] = 0;  
                                p[x + 2 * stride + 3 + i] = 0; 
                                p[x + 2 * stride + 6 + i] = 0; 


                                if (color > 0.1 * 2295)
                                    p[x + stride + 3 + i] = 255;  
                                if (color > 0.2 * 2295)
                                    p[x + stride + 6 + i] = 255; 
                                if (color > 0.3 * 2295)
                                    p[x + 3 + i] = 255;  
                                if (color > 0.4 * 2295)
                                    p[x + stride + i] = 255;  
                                if (color > 0.5 * 2295)
                                    p[x + stride + i] = 255;  
                                if (color > 0.6 * 2295)
                                    p[x + 2 * stride + 6 + i] = 255; 
                                if (color > 0.7 * 2295)
                                    p[x + 6 + i] = 255; 
                                if (color > 0.8 * 2295)
                                    p[x + i] = 255;  
                                if (color > 0.9 * 2295)
                                    p[x + 2 * stride + 3 + i] = 255; 
                            }
                        }
                        p = firstPixel + (y * bmData.Stride);
                    }
                }
            }

            img.UnlockBits(bmData);
            return true;
        }
    }

   }
